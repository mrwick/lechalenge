"""ex5.py

basically a copy of the REPL used to explore the data
with only basic knowledge of pandas, it was discovered
that the points were string representations of python lists
or something

so. . used eval, but probably something exists to transform that

would have loved to get it into matplotlib
.. and discover ply/meshlab/cloudcompare

"""
from settings import Settings

import numpy
import pandas

settings = Settings()

df = pandas.read_csv(settings.cant_csv)
cant_array = numpy.array(df["cant"])
cant_array.min()
cant_col = df["cant"]
cant_col.min()
cant_col.mean()
points_col = df["Points"]
p0l = eval(points_col[0])
p0a = numpy.array(p0l)
p0a_mean = p0a.mean()
print(f"p0a_mean: {p0a_mean}")
